-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.13-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for inventory_depot_air
DROP DATABASE IF EXISTS `inventory_depot_air`;
CREATE DATABASE IF NOT EXISTS `inventory_depot_air` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `inventory_depot_air`;

-- Dumping structure for table inventory_depot_air.admin
DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `kd_admin` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL DEFAULT '',
  `nama` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`kd_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table inventory_depot_air.admin: ~1 rows (approximately)
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
REPLACE INTO `admin` (`kd_admin`, `email`, `nama`, `password`, `level`, `status`) VALUES
	(1, 'admin@air.com', 'Admin', '123', 1, 1);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;

-- Dumping structure for table inventory_depot_air.barang
DROP TABLE IF EXISTS `barang`;
CREATE TABLE IF NOT EXISTS `barang` (
  `kd_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(50) NOT NULL DEFAULT '',
  `harga_beli` int(11) NOT NULL DEFAULT 0,
  `harga_jual` int(11) NOT NULL DEFAULT 0,
  `satuan` varchar(50) NOT NULL DEFAULT '',
  `stok` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`kd_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table inventory_depot_air.barang: ~3 rows (approximately)
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
REPLACE INTO `barang` (`kd_barang`, `nama_barang`, `harga_beli`, `harga_jual`, `satuan`, `stok`) VALUES
	(1, 'Air', 225, 200, 'Liter', '1582'),
	(2, 'Tutup Galon', 25, 220, 'Pcs', '178'),
	(3, 'Tisue Galon', 25, 30, 'Pcs', '78');
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;

-- Dumping structure for table inventory_depot_air.pembelian
DROP TABLE IF EXISTS `pembelian`;
CREATE TABLE IF NOT EXISTS `pembelian` (
  `kd_pembelian` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_pembelian` datetime DEFAULT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `harga_barang` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `kd_admin` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_pembelian`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table inventory_depot_air.pembelian: ~5 rows (approximately)
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;
REPLACE INTO `pembelian` (`kd_pembelian`, `tgl_pembelian`, `nama_barang`, `harga_barang`, `jumlah`, `kd_admin`) VALUES
	(1, '2020-07-05 23:32:42', 'air', 1000, 1, 1),
	(2, '2020-07-05 23:32:42', 'air', 1000, 1, 1),
	(3, '2020-07-08 19:09:10', 'Air', 5200000, 1200, 1),
	(4, '2020-07-08 20:58:34', 'Air', 200000, 1000, 1),
	(5, '2020-07-08 20:59:24', 'Tutup', 22000, 100, 1);
/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;

-- Dumping structure for table inventory_depot_air.penjualan
DROP TABLE IF EXISTS `penjualan`;
CREATE TABLE IF NOT EXISTS `penjualan` (
  `kd_penjualan` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_penjualan` datetime DEFAULT NULL,
  `t_harga_beli` int(11) DEFAULT NULL,
  `t_harga_jual` int(11) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `total_penjualan` int(11) DEFAULT NULL,
  `kd_admin` int(11) DEFAULT NULL,
  PRIMARY KEY (`kd_penjualan`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table inventory_depot_air.penjualan: ~11 rows (approximately)
/*!40000 ALTER TABLE `penjualan` DISABLE KEYS */;
REPLACE INTO `penjualan` (`kd_penjualan`, `tgl_penjualan`, `t_harga_beli`, `t_harga_jual`, `jumlah`, `total_penjualan`, `kd_admin`) VALUES
	(1, '0000-00-00 00:00:00', 4325, 4810, 1, 4810, 1),
	(2, '2020-07-05 22:13:03', 4325, 4810, 2, 9620, 1),
	(3, '2020-07-05 22:14:09', 4325, 4810, 3, 14430, 1),
	(4, '2020-07-05 22:18:19', 4325, 4810, 10, 48100, 1),
	(5, '2020-07-05 22:19:39', 4325, 4810, 10, 48100, 1),
	(6, '2020-07-05 22:20:02', 4325, 4810, 10, 48100, 1),
	(7, '2020-07-05 22:22:25', 4325, 4810, 10, 48100, 1),
	(8, '2020-07-05 22:31:43', 4325, 4810, 10, 48100, 1),
	(9, '2020-07-05 22:32:10', 4325, 4810, 10, 48100, 1),
	(10, '2020-07-05 22:32:58', 4325, 4810, 1, 4810, 1),
	(11, '2020-07-05 23:01:02', 4325, 4810, 1, 4810, 1);
/*!40000 ALTER TABLE `penjualan` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
