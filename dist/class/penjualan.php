<?php 
include "database.php";
class DbPenjualan{

	function __construct(){
        $this->db = new Db();
    }
    
    function tampilData()
	{
		$data = mysqli_query($this->db->koneksi,"SELECT * FROM penjualan order by tgl_penjualan desc");
		
		if(mysqli_num_rows($data)== 0){
			// echo "Data Not Found";
		}
		else{
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}
    }
    
    function tampilDataHargaAir()
	{
		$data = mysqli_query($this->db->koneksi,"SELECT a.harga_jual, a.harga_beli, a.stok FROM barang a WHERE kd_barang = 1");
		
		if(mysqli_num_rows($data)== 0){
			// echo "Data Not Found";
		}
		else{
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}
    }
    
    function tampilDataHargaTutup()
	{
		$data = mysqli_query($this->db->koneksi,"SELECT a.harga_jual, a.harga_beli, a.stok FROM barang a WHERE kd_barang = 2");
		
		if(mysqli_num_rows($data)== 0){
			// echo "Data Not Found";
		}
		else{
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}
    }
    
    function tampilDataHargaTisue()
	{
		$data = mysqli_query($this->db->koneksi,"SELECT a.harga_jual, a.harga_beli, a.stok FROM barang a WHERE kd_barang = 3");
		
		if(mysqli_num_rows($data)== 0){
			// echo "Data Not Found";
		}
		else{
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}
    }
    
    function tampilDataHarga()
	{
		$data = mysqli_query($this->db->koneksi,"SELECT a.harga_jual, a.harga_beli, a.stok FROM barang a WHERE kd_barang = 3");
		
		if(mysqli_num_rows($data)== 0){
			// echo "Data Not Found";
		}
		else{
			while($row = mysqli_fetch_array($data)){
				$hasil[] = $row;
			}
			return $hasil;
		}
	}

	function tambah_data($date,$harga_jual,$harga_beli,$jumlah,$total,$kdAdmin){
		$query = "INSERT INTO penjualan (tgl_penjualan, t_harga_jual, t_harga_beli, jumlah, total_penjualan, kd_admin) VALUES (NOW(),'$harga_jual','$harga_beli','$jumlah','$total','$kdAdmin')";
		mysqli_query($this->db->koneksi, $query);
	}

	function update_air($stok){
		$query = "UPDATE barang SET `stok`=`stok`-'$stok' WHERE kd_barang=1";
		mysqli_query($this->db->koneksi,$query);
	}

	function update_tutup($stok){
		$query = "UPDATE barang SET `stok`=`stok`-'$stok' WHERE kd_barang=2";
		mysqli_query($this->db->koneksi, $query);
	}

	function update_tisue($stok){
		$query = "UPDATE barang SET `stok`=`stok`-'$stok' WHERE kd_barang=3";
		mysqli_query($this->db->koneksi, $query);
	}

	function delete($kd_admin){
		$query = "DELETE FROM admin WHERE kd_admin='$kd_admin'";
		mysqli_query($this->db->koneksi,$query);
	}

} 


?>