<?php  
    class Db {
        public $host = "localhost";
        public $username = "root";
        public $pass = "";
        public $database = "inventory_depot_air";

        function __construct(){
            $this->koneksi = mysqli_connect($this->host, $this->username, $this->pass, $this->database);
            if (mysqli_connect_errno()){
                echo "Koneksi database gagal : " . mysqli_connect_error();
            }
        }

        function set_db_host($host) {
            $this->host = $host;
        }

        function get_db_host() {
            return $this->host;
        }

        function set_db_username($username) {
            $this->username = $username;
        }

        function get_db_username() {
            return $this->username;
        }

        function set_db_password($pass) {
            $this->pass = $pass;
        }

        function get_db_password() {
            return  $this->pass;
        }

        function set_db_database($database) {
            $this->database = $database;
        }

        
        function get_db_database() {
            return $this->database;
        }
    }
    $db = new Db();
?>