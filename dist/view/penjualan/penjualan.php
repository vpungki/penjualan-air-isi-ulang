<?php 
    include '././class/penjualan.php';
    $dbPenjualan = new DbPenjualan();
    $hargaBeli = 0;
    $hargaJual = 0;
    $stokAir = 0;
    $stokTutup = 0;
    $stokTisue = 0;
?>
<div class="container-fluid">
    <h1 class="mt-4">Penjualan</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active">Penjualan</li>
    </ol>
    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    History Penjualan
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Total Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                    if (is_array($dbPenjualan->tampilData()) || is_object($dbPenjualan->tampilData())){
                                    foreach($dbPenjualan->tampilData() as $x){
                                   
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo date('d F Y H:i:s', strtotime($x['tgl_penjualan'])); ?></td>
                                    <td><?php echo $x['t_harga_jual']; ?></td>
                                    <td><?php echo $x['jumlah']; ?></td>
                                    <td><?php echo $x['total_penjualan']; ?></td>
                                </tr>
                                <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Penjualan
                </div>
                <div class="card-body">
                    <form role="form" method="post" action="./index.php?page=penjualan_save&action=add">
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Harga Air / Liter</label>
                            <div class="col-sm-8">
                                <?php
                                    if (is_array($dbPenjualan->tampilDataHargaAir()) || is_object($dbPenjualan->tampilDataHargaAir())){
                                    foreach($dbPenjualan->tampilDataHargaAir() as $x){
                                        $hargaJual = $x['harga_jual'] * 19;
                                        $hargaBeli = $x['harga_beli'] * 19;
                                        $stokAir = $x['stok'];
                                        echo "<label>Rp. ".$x['harga_jual']." /Liter</label>";
                                    }
                                } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Harga Tutup / Pcs</label>
                            <div class="col-sm-8">
                                <?php
                                    if (is_array($dbPenjualan->tampilDataHargaTutup()) || is_object($dbPenjualan->tampilDataHargaTutup())){
                                    foreach($dbPenjualan->tampilDataHargaTutup() as $x){
                                        $hargaJual = $hargaJual + $x['harga_jual'];
                                        $hargaBeli = $hargaBeli + $x['harga_beli'];
                                        $stokTutup = $x['stok'];
                                        echo "<label>Rp. ".$x['harga_jual']." /Pcs</label>";
                                    }
                                } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Harga Tisue / Pcs</label>
                            <div class="col-sm-8">
                                <?php
                                    if (is_array($dbPenjualan->tampilDataHargaTisue()) || is_object($dbPenjualan->tampilDataHargaTisue())){
                                    foreach($dbPenjualan->tampilDataHargaTisue() as $x){
                                        $hargaJual = $hargaJual + $x['harga_jual'];
                                        $hargaBeli = $hargaBeli + $x['harga_beli'];
                                        $stokTisue = $x['stok'];
                                        echo "<label>Rp. ".$x['harga_jual']." /Pcs</label>";
                                    }
                                } ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Harga</label>
                            <div class="col-sm-8">
                                <?php
                                    echo "<label>Rp. ".$hargaJual."</label>";
                                ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jumlah" class="col-sm-4 col-form-label">Jumlah</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" name="jumlah" id="jumlah" onchange="totalHarga(<?php echo $hargaJual; ?>)">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-success" type="button"
                                            onclick="totalHarga(<?php echo $hargaJual; ?>)">Proses</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Harga Total</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp. </span>
                                    </div>
                                    <input type="text" class="form-control" readonly name="total" id="total" value="">
                                </div>
                            </div>
                        </div>
                        <input type="text" name="t_harga_jual" value="<?php echo $hargaJual; ?>" hidden>
                        <input type="text" name="t_harga_beli" value="<?php echo $hargaBeli; ?>" hidden>
                        <input type="text" name="stokAir" value="<?php echo $stokAir; ?>" hidden>
                        <input type="text" name="stokTutup" value="<?php echo $stokTutup; ?>" hidden>
                        <input type="text" name="stokTisue" value="<?php echo $stokTisue; ?>" hidden>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                            <button type="submit" class="btn btn-success" id="byrBtn" disabled>Bayar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
function totalHarga(hrg) {
    // var jmh = parseInt(document.getElementById('jumlah').value;
    // if(typeof(jmh) == 'number'){
    //     console.log(jmh)
    // }else{
    //     console.log(num)
    // }
    var jumlah_harga = hrg * parseInt(document.getElementById('jumlah').value);
    if(isNaN(jumlah_harga)){
        document.getElementById('total').value = 0;
    }else{
        document.getElementById('total').value = jumlah_harga;
    }

    if(document.getElementById('total').value==0){
        document.getElementById("byrBtn").disabled = true;
    }else{
        document.getElementById("byrBtn").disabled = false;
    }
}
</script>