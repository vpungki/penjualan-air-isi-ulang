<?php 
    include './class/tutup.galon.php';
    $koneksi = new DbTutupGalon();

    $action = $_GET['action'];

    if($action == "add"){
        $stok = $_POST['stok'];

        if(is_numeric($stok)){
            $koneksi->tambah_data($stok);
           echo "
                <script> document.location='index.php?page=ttpg';</script>
           ";
            
        }else{
            echo "
                <script> document.location='index.php?page=ttpg_add'; alert('hanya angka')</script>
            ";
        }
    }if($action == "edit"){
        $harga_jual = $_POST['harga_jual'];
        $harga_beli = $_POST['harga_beli'];

        if(is_numeric($harga_jual) && is_numeric($harga_beli)){
            $koneksi->update_data($harga_jual, $harga_beli);
            echo "
                <script> document.location='index.php?page=ttpg';</script>
        ";
            
        }else{
            echo "
                <script> document.location='index.php?page=ttpg_edit'; alert('hanya angka')</script>
            ";
        }
    }
    
?>