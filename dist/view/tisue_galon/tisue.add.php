<div class="container-fluid">
    <h1 class="mt-4">Tisue Galon</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="index.php?page=tsug">Tisue Galon</a></li>
        <li class="breadcrumb-item active">Tambah</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Tisue Galon
        </div>
        <div class="card-body">
            <form role="form" method="post" action="./index.php?page=tsug_save&action=add">
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputName1">Stok</label>
                        <input type="text" class="form-control" id="exampleInputName1" name="stok"
                            placeholder="Enter Tisue Galon" required>
                            <small class>*Satuan Pcs</small>
                    </div>

                </div>
                <!-- /.card-body -->

                    <button type="submit" class="btn btn-success">Simpan</button>
                    <a href="index.php?page=tsug" class="btn float-right btn-default">Back</a>
               
            </form>
        </div>
    </div>
</div>