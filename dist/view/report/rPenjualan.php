<?php
ob_start();
$str = $_POST['tglReport2'];
$arrStr=explode("/",$str);

include '../../class/report.php';
$db = new DbReport();

require('../../src/fpdf/fpdf.php');

$pdf = new FPDF('L','mm','A4');
$pdf->addPage();
$pdf->setAutoPageBreak(true);
$pdf->setFont('Arial','',12);
$pdf->text(95,10,'LAPORAN DATA HASIL PENJUALAN BULAN Ke-'.$arrStr[0].' '.$arrStr[1].'');
$pdf->Line(5,15,290,15);
$yi = 50;
$ya = 44;
$i = 1;
$no = 1;
$max = 31;
$row = 3;
$pdf->setFont('Arial','',9);
$pdf->setFillColor(222,222,222);
$pdf->setXY(15,$ya);
$pdf->CELL(6,6,'NO',1,0,'C',1);
$pdf->CELL(50,6,'Harga Beli',1,0,'C',1);
$pdf->CELL(50,6,'Harga Jual',1,0,'C',1);
$pdf->CELL(50,6,'Jumlah',1,0,'C',1);
$pdf->CELL(50,6,'Total Harga',1,0,'C',1);
$pdf->CELL(50,6,'Keuntungan',1,0,'C',1);
$ya = $yi + $row;
$nos = 1;
$sss = 40;
$ccc = 5;

$bulan = $arrStr[0];
$tahun = $arrStr[1];
if (is_array($db->reportPenjualan($bulan,$tahun)) || is_object($db->reportPenjualan($bulan,$tahun))){
    foreach($db->reportPenjualan($bulan,$tahun) as $data){
    $nos++;
    $wey = ($nos*$ccc) + $sss;
    $pdf->setXY(15,$wey);
    $pdf->setFont('arial','',8);
    $pdf->setFillColor(255,255,255);
    $pdf->cell(6,13,$no,1,0,'C',1);
    $pdf->cell(50,13,"Rp. ".$data['t_harga_beli'],1,0,'C',1);
    $pdf->cell(50,13,"Rp. ".$data['t_harga_jual'],1,0,'C',1);
    $pdf->CELL(50,13,$data['jumlah'],1,0,'C',1);
    $pdf->CELL(50,13,"Rp. ".$data['total_penjualan'],1,0,'C',1);
    $pdf->CELL(50,13,"Rp. ".(($data['t_harga_jual']-$data['t_harga_beli'])*$data['jumlah']),1,0,'C',1);
    $ya = $ya+$row;
    $nos++;
    $no++;
    $i++;
    }
}
// $pdf->text(210,$ya+50,"asdf");
// $pdf->text(210,$ya+50,"Purbalingga , ". date('d-M-Y'));
// $pdf->text(210,$ya+55,"Program Kotaku Kabupaten Purbalingga");
// $pdf->text(222,$ya+75,"Teguh Yuliarto,S.P.");
// $pdf->text(225,$ya+80,"Askot Mandiri");
$pdf->Output();
?>