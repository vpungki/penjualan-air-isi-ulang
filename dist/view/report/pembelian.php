<?php
// echo $_POST['tglReport'];
include '././class/report.php';
$dbReport = new DbReport();
$str = "";
$arrStr = "00/0000";
$cek = 0;
if (!empty($_POST["tglReport"])) {
    // echo "Yes, mail is set";    
    $str = $_POST['tglReport']; 
    $arrStr=explode("/",$str);
    $cek = 1;
} else {  
    // echo "No, mail is not set";
    $cek = 0;
    $arrStr = "00/0000";
}

// $str = $_POST['tglReport'];
// $arrStr=explode("/",$str);
// echo $str;
// echo $arrStr;
?>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        History Penjualan
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Barang</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                        if ($cek == 1){
                        foreach($dbReport->reportPembelian($arrStr[0], $arrStr[1]) as $x){
                        
                    ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $x['nama_barang']; ?></td>
                        <td><?php echo $x['jumlah']; ?></td>
                        <td><?php echo $x['harga_barang']; ?></td>
                    </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
        <form role="form" method="post" action="view/report/rPembelian.php" autocomplete="off">
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" id="dtReport" name="tglReport" value="<?php echo $str; ?>" required hidden> 
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Print</button>
        </form>
    </div>
</div>