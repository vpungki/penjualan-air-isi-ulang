<?php
// echo $_POST['tglReport'];
// include '././class/report.php';
// $dbReport = new DbReport();
// $str = "";
// $arrStr = "00/0000";
// $cek = 0;
// if (!empty($_POST["tglReport"])) {
//     // echo "Yes, mail is set";    
//     $str = $_POST['tglReport']; 
//     $arrStr=explode("/",$str);
//     $cek = 1;
// } else {  
//     // echo "No, mail is not set";
//     $cek = 0;
//     $arrStr = "00/0000";
// }

// $str = $_POST['tglReport'];
// $arrStr=explode("/",$str);
// echo $str;
// echo $arrStr;
?>

<div class="card mb-4">
    <div class="card-header">
        <i class="fas fa-table mr-1"></i>
        History Penjualan
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Harga Beli</th>
                        <th>Harga Jual</th>
                        <th>Jumlah</th>
                        <th>Total Harga</th>
                        <th>Keuntungan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                        if ($cek == 1){
                        foreach($dbReport->reportPenjualan($arrStr[0], $arrStr[1]) as $x){
                        
                    ?>
                    <tr>
                        <td><?php echo $no++; ?></td>
                        <td><?php echo $x['t_harga_beli']; ?></td>
                        <td><?php echo $x['t_harga_jual']; ?></td>
                        <td><?php echo $x['jumlah']; ?></td>
                        <td><?php echo $x['total_penjualan']; ?></td>
                        <td><?php echo (($x['t_harga_jual']-$x['t_harga_beli'])*$x['jumlah']); ?></td>
                    </tr>
                    <?php }
                } ?>
                </tbody>
            </table>
        </div>
        <form role="form" method="post" action="view/report/rPenjualan.php" autocomplete="off">
            <div class="form-group">
                <div class="input-group date">
                    <input type="text" class="form-control" id="dtReport" name="tglReport2" value="<?php echo $str; ?>" required hidden> 
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Print</button>
        </form>
    </div>
</div>