<div class="container-fluid">
    <h1 class="mt-4">Laporan</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active">Laporan</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Laporan
        </div>
        <div class="card-body">
            <div class="card">
                <div class="card-body">
                    <form role="form" method="post" action="" autocomplete="off">
                        <div class="form-group">
                            <label for="dtReport">Bulan</label>
                            <div class="input-group date">
                                <input type="text" class="form-control" id="dtReport" name="tglReport" required> 
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group date">
                                <!-- <a class="btn btn-light" id="preR1">Preview</a> -->
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Preview</button>
                    </form>
                </div>
            </div>
                <br>
            <div id="vw1">
                <?php include"pembelian.php"; ?>
            </div>
            <br>
            <div id="vw1">
                <?php include"penjualan.php"; ?>
            </div>
        </div>
    </div>

</div>