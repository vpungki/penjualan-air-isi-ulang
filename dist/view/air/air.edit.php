<?php 
    include '././class/air.php';
    $dbAir = new DbAir();
  
    $data = $dbAir->getById();
?>
<div class="container-fluid">
    <h1 class="mt-4">Air</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="index.php?page=air">Air</a></li>
        <li class="breadcrumb-item active">Edit</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Air
        </div>
        <div class="card-body">
            <form role="form" method="post" action="./index.php?page=air_save&action=edit">
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputName1">Harga Beli *</label>
                        <input type="text" class="form-control" id="exampleInputName1" name="harga_beli"
                            placeholder="Enter Harga Air" value="<?php echo $data['harga_beli'] ?>" required hidden>
                        <input type="text" class="form-control" id="exampleInputName1" name="harga_beli"
                            placeholder="Enter Harga Air" value="<?php echo $data['harga_beli'] ?>" required disabled>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputName1">Harga Jual *</label>
                        <input type="text" class="form-control" id="exampleInputName1" name="harga_jual"
                            placeholder="Enter Harga Air" value="<?php echo $data['harga_jual'] ?>" required>
                    </div>
                </div>
                <!-- /.card-body -->

                <button type="submit" class="btn btn-success">Simpan</button>
                <a href="index.php?page=air" class="btn float-right btn-default">Back</a>

            </form>
        </div>
    </div>
</div>