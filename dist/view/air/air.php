<?php 
    include '././class/air.php';
    $dbAir = new DbAir();
?>
<div class="container-fluid">
    <h1 class="mt-4">Air</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active">Air</li>
    </ol>
    <div class="">
        <!-- <a href="index.php?page=air_add" class="btn btn-success" style="margin-bottom:10px;"><i class="fas fa-plus"></i>
            Tambah</a> -->
    </div>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Air
        </div>
        <div class="card-body">
        <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Harga Jual</th>
                            <th>Harga Beli</th>
                            <th>Stock</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (is_array($dbAir->tampilData()) || is_object($dbAir->tampilData())){
                            foreach($dbAir->tampilData() as $x){
                        ?>
                        <tr>
                            <td>Rp. <?php echo $x['harga_jual']; ?> Per Liter</td>
                            <td>Rp. <?php echo $x['harga_beli']; ?> Per Liter</td>
                            <td><?php echo $x['stok']; ?> Liter</td>
                            <td>
                                <a href="index.php?page=air_edit" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit"><i
                                        class="fa fa-edit"></i></a>
                            </td>
                        </tr>
                        <?php }
                            } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>