<?php 
    include './class/air.php';
    $koneksi = new DbAir();

    $action = $_GET['action'];

    if($action == "add"){
        $stok = $_POST['stok'];

        if(is_numeric($stok)){
            $koneksi->tambah_data($stok);
           echo "
                <script> document.location='index.php?page=air';</script>
           ";
            
        }else{
            echo "
                <script> document.location='index.php?page=air_add'; alert('hanya angka')</script>
            ";
        }
    }if($action == "edit"){
        $harga_jual = $_POST['harga_jual'];
        $harga_beli = $_POST['harga_beli'];

        if(is_numeric($harga_jual)){
            $koneksi->update_data($harga_jual, $harga_beli);
            echo "
                <script> document.location='index.php?page=air';</script>
        ";
            
        }else{
            echo "
                <script> document.location='index.php?page=air_edit'; alert('hanya angka')</script>
            ";
        }
    }
    
?>