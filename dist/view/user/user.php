<?php 
    include '././class/user.php';
    $dbUser = new DbUser();
?>
<div class="container-fluid">
    <h1 class="mt-4">User</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active">User</li>
    </ol>
    <div class="">
        <a href="index.php?page=user_add" class="btn btn-success" style="margin-bottom:10px;"><i
                class="fas fa-plus"></i> Tambah</a>
    </div>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            User
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                            if (is_array($dbUser->tampilData()) || is_object($dbUser->tampilData())){
                            foreach($dbUser->tampilData() as $x){
                        ?>
                        <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $x['nama']; ?></td>
                            <td><?php echo $x['email']; ?></td>
                            <td>
                                <?php
                                  if($x['level']==1){
                                    echo 'Administrator';
                                  }else{
                                    echo 'Kasir';
                                  }
                                ?>
                            </td>
                            <td>
                                <?php
                                  if($x['status']==1){
                                    echo '<span class="badge bg-success" selected="false">Active</span>';
                                  }else{
                                    echo '<span class="badge bg-danger">Non Active</span>';
                                  }
                                ?>
                            </td>
                            <td>
                                <a href="index.php?page=user_edit&id=<?php echo $x['kd_admin']; ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Edit"><i
                                        class="fa fa-edit"></i></a>
                                <a href="index.php?page=user_del&action=del&id=<?php echo $x['kd_admin']; ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Hapus"><i
                                        class="fas fa-times"></i></a>
                            </td>
                        </tr>
                        <?php }
                            } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>