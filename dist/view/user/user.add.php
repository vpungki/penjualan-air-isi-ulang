<div class="container-fluid">
    <h1 class="mt-4">User</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active"><a href="index.php?page=user">User</a></li>
        <li class="breadcrumb-item active">Tambah</li>
    </ol>
    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            User
        </div>
        <div class="card-body">
            <form role="form" method="post" action="./index.php?page=user_save&action=add">
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputName1">Nama *</label>
                        <input type="text" class="form-control" id="exampleInputName1" name="nama"
                            placeholder="Enter Nama" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email *</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" name="email"
                            placeholder="Enter Email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Password *</label>
                        <input type="password" class="form-control" id="exampleInputPassword1" name="pass"
                            placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword2">Re Password *</label>
                        <input type="password" class="form-control" id="exampleInputPassword2" name="repass"
                            placeholder="Password" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Level</label>
                        <div class="input-group">
                            <select class="form-control select2" name="level" style="width: 100%;" required>
                                <option value="" disabled selected>None</option>
                                <option value="1" >Administrator</option>
                                <option value="2" >Kasir</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Status</label>
                        <div class="input-group">
                            <select class="form-control select2" name="status" style="width: 100%;" required>
                                <option value="1" >Active</option>
                                <option value="0" >Non Active</option>
                            </select>
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->

                    <button type="submit" class="btn btn-success">Simpan</button>
                    <a href="index.php?page=user" class="btn float-right btn-default">Back</a>
               
            </form>
        </div>
    </div>
</div>