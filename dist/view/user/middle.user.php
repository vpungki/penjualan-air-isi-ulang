<?php 
    include './class/user.php';
    $koneksi = new DbUser();

    $action = $_GET['action'];

    if($action == "add"){
        $nama = $_POST['nama'];
        $email = $_POST['email'];
        $pass= $_POST['pass'];
        $repass = $_POST['repass'];
        $level = $_POST['level'];
        $status = $_POST['status'];

        if($pass == $repass){
            $koneksi->tambah_data($nama, $email, $pass, $level, $status);
           echo "
                <script> document.location='index.php?page=user';</script>
           ";
            
        }else{
            echo "
                <script> document.location='index.php?page=user_add';</script>
            ";
        }
    }if($action == "edit"){
        $kd_admin = $_POST['kd_admin'];
        $nama = $_POST['nama'];
        $email = $_POST['email'];
        $pass= $_POST['pass'];
        $repass = $_POST['repass'];
        $level = $_POST['level'];
        $status = $_POST['status'];

        if($pass == $repass){
            $koneksi->update_data($kd_admin, $nama, $email, $pass, $level, $status);
            echo "
                <script> document.location='index.php?page=user';</script>
        ";
            
        }else{
            echo "
                <script> document.location='index.php?page=user_edit';</script>
            ";
        }
    }if($action == "del"){
        $id_user_del = $_GET['id'];
        $koneksi->delete($id_user_del);
        echo "
            <script> document.location='index.php?page=user';</script>
        ";
    }
    
?>