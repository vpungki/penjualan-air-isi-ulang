<?php 
    include '././class/pembelian.php';
    $dbPembelian = new DbPembelian();
   
?>
<div class="container-fluid">
    <h1 class="mt-4">Pembelian</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active">Pembelian</li>
    </ol>
    <div class="row">
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    History Pembelian
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Transaksi</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                if (is_array($dbPembelian->tampilData()) || is_object($dbPembelian->tampilData())){
                                    foreach($dbPembelian->tampilData() as $x){
                                   
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo date('d F Y H:i:s', strtotime($x['tgl_pembelian'])); ?></td>
                                    <td><?php echo $x['nama_barang']; ?></td>
                                    <td><?php echo $x['harga_barang']; ?></td>
                                    <td><?php echo $x['jumlah']; ?></td>
                                </tr>
                                <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Pembelian
                </div>
                <div class="card-body">
                    <form role="form" method="post" action="./index.php?page=pembelian_save&action=add">
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Nama</label>
                            <div class="col-sm-8">
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="optradio" id="id1"
                                            value="Air">Air
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="optradio" id="id2"
                                            value="Tutup">Tutup Galon
                                    </label>
                                </div>
                                <div class="form-check-inline disabled">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="optradio" id="id3"
                                            value="Tisue">Tisue Galon
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jumlah" class="col-sm-4 col-form-label">Jumlah</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <input type="number" min="0" class="form-control" name="jumlah" id="jumlah" require>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="lbl">Ltr</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label">Harga</label>
                            <div class="col-sm-8">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Rp. </span>
                                    </div>
                                    <input type="number" min="0" class="form-control" name="total" id="total" value="" require>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success" id="byrBtn" onclick(cek())>Bayar</button>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-4 col-form-label"></label>
                            <div class="col-sm-8">
                                <span id="errorInput"></span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>