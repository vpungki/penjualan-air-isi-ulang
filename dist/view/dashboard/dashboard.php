<?php 
    include '././class/dashboard.php';
    $dbDashborad = new DbDashboard();
?>
<div class="container-fluid">
    <h1 class="mt-4">Dashboard</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Dashboard</li>
    </ol>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 p-2">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <?php
                        $no = 1;
                        if (is_array($dbDashborad->tampilDataAir()) || is_object($dbDashborad->tampilDataAir())){
                            foreach($dbDashborad->tampilDataAir() as $x){
                                echo "<h1>".$x['stok']." Liter</h1>";
                            }
                        } ?>
                    <hr />
                    <h2 class="card-title display-1" style="font-size:3.0vmin;">Stock Air</h2>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 p-2">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <?php
                        $no = 1;
                        if (is_array($dbDashborad->tampilDataTutup()) || is_object($dbDashborad->tampilDataTutup())){
                            foreach($dbDashborad->tampilDataTutup() as $x){
                                echo "<h1>".$x['stok']." Pcs</h1>";
                            }
                        } ?>
                    <hr />
                    <h2 class="card-title display-1" style="font-size:3.0vmin;">Stock Tutup Galon</h2>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4 p-2">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <?php
                        $no = 1;
                        if (is_array($dbDashborad->tampilDataTisue()) || is_object($dbDashborad->tampilDataTisue())){
                            foreach($dbDashborad->tampilDataTisue() as $x){
                                echo "<h1>".$x['stok']." Pcs</h1>";
                            }
                        } ?>
                    <hr />
                    <h2 class="card-title display-1" style="font-size:3.0vmin;">Stock Tisue Galon</h2>
                </div>
            </div>
        </div>
        <!-- <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 p-2">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <h1>1245</h1>
                    <hr />
                    <h2 class="card-title display-1" style="font-size:3.0vmin;">Fixed Deposit</h2>
                </div>
            </div>
        </div> -->
    </div>
</div>