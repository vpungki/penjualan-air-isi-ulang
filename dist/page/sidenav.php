<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="index.php">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>
                <div class="sb-sidenav-menu-heading">Master</div>
                <a class="nav-link" href="index.php?page=air">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Air
                </a>
                <a class="nav-link" href="index.php?page=ttpg">
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Tutup Galon
                </a>
                <a class="nav-link" href="index.php?page=tsug" >
                    <div class="sb-nav-link-icon"><i class="fas fa-book-open"></i></div>
                    Tisue Galon
                </a>
                <div class="sb-sidenav-menu-heading">Transaksi</div>
                <a class="nav-link" href="index.php?page=pnjln">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Penjualan
                </a>
                <a class="nav-link" href="index.php?page=pmbln">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Pembelian
                </a>
                <!-- <a class="nav-link" href="tables.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Pembelian
                </a> -->
                <div class="sb-sidenav-menu-heading">Laporan</div>
                <a class="nav-link" href="index.php?page=report">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    Keuangan 
                </a>
                <!-- <a class="nav-link" href="tables.html">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Barang
                </a> -->
                <div class="sb-sidenav-menu-heading">Setting</div>
                <a class="nav-link" href="index.php?page=user">
                    <div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>
                    User 
                </a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            <?php echo $_SESSION['login_admin']['levelName']; ?>
        </div>
    </nav>
</div>