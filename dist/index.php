<?php 
    session_start();
    if(! isset($_SESSION['login_admin']))
    {
      header('location:login.php');
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - Toko Galon</title>
    <link href="src/css/styles.css" rel="stylesheet" />
    <link rel="stylesheet" href="src/css/bootstrap.min.css"
        integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="src/css/bootstrap-datepicker.css">
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet"
        crossorigin="anonymous" />
    
</head>

<body class="sb-nav-fixed">
    <?php include 'page/navbar.php'; ?>
    <div id="layoutSidenav">
        <?php include 'page/sidenav.php'; ?>
        <div id="layoutSidenav_content">
            <main>
                <?php  
                    if (isset($_GET['page'])) {
                        if ($_GET['page']=="user") {
                            include 'view/user/user.php';
                        }
                        elseif ($_GET['page']=="user_add") {
                            include 'view/user/user.add.php';
                        }
                        elseif ($_GET['page']=="user_edit") {
                            include 'view/user/user.edit.php';
                        }
                        elseif ($_GET['page']=="user_del") {
                            include 'view/user/middle.user.php';
                        }
                        elseif ($_GET['page']=="user_save") {
                            include 'view/user/middle.user.php';
                        }
                        elseif ($_GET['page']=="air") {
                            include 'view/air/air.php';
                        }
                        elseif ($_GET['page']=="air_add") {
                            include 'view/air/air.add.php';
                        }
                        elseif ($_GET['page']=="air_edit") {
                            include 'view/air/air.edit.php';
                        }
                        elseif ($_GET['page']=="air_save") {
                            include 'view/air/middle.air.php';
                        }
                        elseif ($_GET['page']=="ttpg") {
                            include 'view/tutup_galon/tutup.php';
                        }
                        elseif ($_GET['page']=="ttpg_add") {
                            include 'view/tutup_galon/tutup.add.php';
                        }
                        elseif ($_GET['page']=="ttpg_edit") {
                            include 'view/tutup_galon/tutup.edit.php';
                        }
                        elseif ($_GET['page']=="ttpg_save") {
                            include 'view/tutup_galon/middle.tutup.php';
                        }
                        elseif ($_GET['page']=="tsug") {
                            include 'view/tisue_galon/tisue.php';
                        }
                        elseif ($_GET['page']=="tsug_add") {
                            include 'view/tisue_galon/tisue.add.php';
                        }
                        elseif ($_GET['page']=="tsug_edit") {
                            include 'view/tisue_galon/tisue.edit.php';
                        }
                        elseif ($_GET['page']=="tsug_save") {
                            include 'view/tisue_galon/middle.tisue.php';
                        }
                        elseif ($_GET['page']=="pnjln") {
                            include 'view/penjualan/penjualan.php';
                        }
                        elseif ($_GET['page']=="penjualan_save") {
                            include 'view/penjualan/middle.penjualan.php';
                        }
                        elseif ($_GET['page']=="pmbln") {
                            include 'view/pembelian/pembelian.php';
                        }
                        elseif ($_GET['page']=="pembelian_save") {
                            include 'view/pembelian/middle.pembelian.php';
                        }
                        elseif ($_GET['page']=="report") {
                            include 'view/report/report.php';
                        }
                        elseif ($_GET['page']=="print") {
                            include 'view/report/rPembelian.php';
                        }
                    }else{
                        include 'view/dashboard/dashboard.php';
                    }
                    
                ?>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Your Website 2020</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    </script>
    <script src="src/js/bootstrap.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js" crossorigin="anonymous">
    </script>
    <script src="src/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js" crossorigin="anonymous">
    </script>
    <script src="src/js/bootstrap-datepicker.js"></script>
    <script src="src/js/scripts.js"></script>
    <script src="src/js/scrp.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="src/assets/demo/chart-area-demo.js"></script>
    <script src="src/assets/demo/chart-bar-demo.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="src/assets/demo/datatables-demo.js"></script>
    <script>
    $("#id1").prop("checked", true);
    $('input[type=radio][name=optradio]').change(function() {
        if (this.value == 'Air') {
            document.getElementById("lbl").innerHTML = "Ltr";
        } else if (this.value == 'Tutup') {
            document.getElementById("lbl").innerHTML = "Pcs";
        } else if (this.value == 'Tisue') {
            document.getElementById("lbl").innerHTML = "Pcs";
        }
    });

    function cek() {
        var a = document.getElementById("jumlah").innerHTML.value();
        if (a == null || a == "" || a == 0) {
            alert("apa")
        }
    }
    $('#dtReport').datepicker({
        format: "mm/yyyy",
        startView: 1,
        minViewMode: 1,
        clearBtn: true,
        autoclose: true,
        orientation: "bottom auto"
    });
    $('#dtReport2').datepicker({
        format: "mm/yyyy",
        startView: 1,
        minViewMode: 1,
        clearBtn: true,
        autoclose: true,
        orientation: "bottom auto"
    });
    
    </script>
</body>

</html>